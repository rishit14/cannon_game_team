package com.example.Cannon_Game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CannonGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(CannonGameApplication.class, args);
	}

}
